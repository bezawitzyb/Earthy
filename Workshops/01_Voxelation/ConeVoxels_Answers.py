import Rhino.Geometry as rg
import math


# Get the bounding box of the brep in question (see https://developer.rhino3d.com/api/RhinoCommon/html/Methods_T_Rhino_Geometry_Brep.htm)
Bbox = Shape.GetBoundingBox(True)

# Find the BoundingBox object in the documentation (see previous link to documentation website & search boundingbox) 
# & find how to get the diagonal
# Find the Width, Length and Height of the diagonal of the bounding box of the Brep
W = Bbox.Diagonal.X
L = Bbox.Diagonal.Y
H = Bbox.Diagonal.Z

# Divide the 3 axes of the bounding box by the dimensions of the voxel. 
# This will give you a U, V, W value along the bounding box.
# Turn this value into an integer by finding the next biggest integer (rounding up)
UC = int(math.ceil(W/xS))
VC = int(math.ceil(L/yS))
WC = int(math.ceil(H/zS))

# Create a container for your distance List
pointList = []
distanceList = []

# The following statements assign the boundingbox and list of distances
# to the outputs (check the data flow after this grasshopper component)
b = Bbox
c = distanceList

# Create a base plane for all the geometry 
# https://developer.rhino3d.com/api/RhinoCommon/html/T_Rhino_Geometry_Plane.htm
# Take a look at the Constructors
bPlane = rg.Plane(Bbox.Min, rg.Vector3d.XAxis, rg.Vector3d.YAxis)

# Due to the fact that all voxels are based on the center points, we need
# to create a shift towards the voxel's center, which is half of each dimension
xShift = 0.5*xS
yShift = 0.5*yS
zShift = 0.5*zS

# Write a for-loop that itereates through all UC, VC, WC values of the bounding box
# Create a point with consideration of the half shift at each UC, VC, WC location
# Store this point in the point list
# Create a closest point; from the brep to the point at the UC, VC, WC value 
# find the distance of the point in the U, V ,W address to the closet point on the brep
# if the point is inside the brep, give it a negative value
# save distances to list
# output both lists
for i in range(0, UC):
    for j in range(0, VC):
        for k in range(0, WC):
            point = bPlane.PointAt(xShift+i*xS, yShift+j*yS, zShift+k*zS)
            pointList.append(point)
            cPoint = Shape.ClosestPoint(point)
            distance = point.DistanceTo(cPoint)
            if not (Shape.IsPointInside(point, 0.1, True)):
                distance = -distance
            distanceList.append(distance)
            
# Assign the pointlist to output a
a=pointList